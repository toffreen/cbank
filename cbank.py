import os
import datetime
import tkinter as tk
from shutil import copyfile

import requests
import rarfile
import pandas as pd
from dbfread import DBF
from openpyxl import load_workbook
from openpyxl.styles import Border, Font, Side, Color, PatternFill, Alignment


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DOWNLOADS_DIR = os.path.expanduser('~/Downloads')
BASE_URL = 'https://www.cbr.ru/vfs/credit/forms/'  # page with 101-form report files

EXCEL_NAME = 'cards_volumes.xlsx'
REF_SHEET_NAME = 'ref'  # sheet with database for banks' ids
REP_SHEET_NAME = 'cards_volumes'  # main sheet with report


ROW = 4  # first row for banks' data
C_PORTS = [963, 2241]  # ids of Sovcombank and Qiwi-Bank
MONTHS = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
YEARS = ['2019', '2020', '2021', '2022', '2023', '2024', '2025']


def month_incr(a):
	if a == '12':
		return '01'
	b = int(a) + 1
	if b <= 9:
		return f'0{b}'
	else:
		return str(b)


def year_incr(month, year):
	if month == '12':
		year_incr = str(int(year) + 1)
	else:
		year_incr = year
	return year_incr


def remove_garbage_files():
	listdir = os.listdir(ROOT_DIR)
	for item in listdir:
		if item.endswith('.rar') or item.endswith('.DBF'):
			os.remove(os.path.join(ROOT_DIR, item))


class FileStatus:
	"""
	Fetches last filled column num in rep sheet and grabs filled month_years.
	"""
	def __init__(self, month_year_new=None):
		self.wb = load_workbook(EXCEL_NAME)
		self.ws = self.wb[REP_SHEET_NAME]
		self.month_year_new = month_year_new

	@property
	def downloaded_months(self):
		"""
		After downloading data for the particular month, we throw 
		this month_year from Dashboard into constructor and append it 
		to the list, so we do not download the same month twice.
		"""
		downloaded_months = []
		for col in self.ws.iter_cols(min_row=2, max_row=2, min_col=4, 
			max_col=10000):
			for cell in col:
				if cell.value is None:
					return downloaded_months
				year_month = str(cell.value).split('-')[:2]
				month_year = (year_month[1], year_month[0])
				downloaded_months.append(month_year)
		if self.month_year_new is not None:
			downloaded_months.append(self.month_year_new)
		return downloaded_months

	@property
	def last_colnum(self):
		cnt = 0
		for col in self.ws.iter_cols(min_row=3, max_row=3, max_col=10000):
			for cell in col:
				if cell.value is None:
					return cnt
				cnt += 1
		return cnt


class Parser:
	"""
	Downloads rar archive from cb site and extracts dbf file from there.
	"""
	def __init__(self, base_month, base_year):
		""" 
		base_month/base_year are month/year of reporting period.
		Rar archive file uses month-after in its name, though.
		"""
		self.file_rar = None
		self.base_month = base_month
		self.base_year = base_year
		self.month = month_incr(base_month)
		self.year = year_incr(base_month, base_year)
		self.RAR_NAME = f'101-{self.year}{self.month}01.rar'
		self.DBF_NAME = f'{self.base_month}{self.base_year}B1.DBF'
		self.status_code = None


	def download_rar(self):
		url = f'{BASE_URL}{self.RAR_NAME}'
		r = requests.get(url)
		self.status_code = r.status_code
		if self.status_code == 200:
			file_rar = f'{ROOT_DIR}{self.RAR_NAME}'
			with open(file_rar, 'wb') as f:
				f.write(r.content)
			self.file_rar = file_rar

	def extract_rar(self):
		if self.file_rar is None:
			return
		rar = rarfile.RarFile(self.file_rar)
		rar.extract(self.DBF_NAME, ROOT_DIR)


class Writer:
	"""
	Loads downloaded dbf into pandas df and fills the sheet.
	"""
	def __init__(self, base_month, base_year):
		self.base_month = base_month
		self.base_year = base_year
		self.month = month_incr(base_month)
		self.year = year_incr(base_month, base_year)
		self.RAR_NAME = f'101-{self.year}{self.month}01.rar'
		self.DBF_NAME = f'{self.base_month}{self.base_year}B1.DBF'

		self.dbf = DBF(self.DBF_NAME, char_decode_errors='ignore')
		self.df = pd.DataFrame(iter(self.dbf))
		self.df_out = pd.read_excel(io=EXCEL_NAME, sheet_name=REF_SHEET_NAME)
		self.wb = load_workbook(EXCEL_NAME)
		self.ws = self.wb[REP_SHEET_NAME]
		self.row = ROW
		self.file_status = FileStatus()
		self.COL = self.file_status.last_colnum + 1
		self.status_code = None

	def _fill_date(self):
		cell = self.ws.cell(row=2, column=self.COL)
		cell.value = datetime.date(year=int(self.base_year), 
			month=int(self.base_month), day=1)
		cell.font = Font(name='Arial', size=6, color='FFFFFF')
		cell.fill = PatternFill(start_color='00245C', 
			end_color='00245C', fill_type='solid')
		cell.number_format = 'mmmm yyyy'
		cell.alignment = Alignment(horizontal='right')
	
	def _fill_colnum(self):
		cell = self.ws.cell(row=3, column=self.COL)
		cell.value = self.COL
		cell.font = Font(name='Arial', size=6)
		cell.alignment = Alignment(horizontal='center', vertical='center')
	
	def _fill_nums(self):
		row = self.row
		for code in self.df_out['code']:
			df_sorted = self.df['ORA'][(self.df['REGN'] == code) & 
						(self.df['NUM_SC'].isin(('45508', '45509')))]
			val = df_sorted.sum()/1000000
			cell = self.ws.cell(row=row, column=self.COL)
			cell.value = val
			cell.number_format = '0.00'
			cell.font = Font(name='Arial', size=8)
			cell.border = Border(top=Side(border_style='hair'), 
				bottom=Side(border_style='hair'))

			cell_id = self.ws.cell(row=row, column=2)
			if cell_id.value in C_PORTS:
				cell.fill = PatternFill(start_color='EEEEEE', 
					end_color='EEEEEE', fill_type='solid')
			row += 1
		self.row = row
	
	def _fill_portfolio_nums(self):
		row = self.row + 2
		for c_port in C_PORTS:
			cell = self.ws.cell(row=row, column=self.COL)
			val = self.df['IITG'][(self.df['REGN'] == c_port) & 
				(self.df['NUM_SC'].isin(('45508', '45509')))]\
				.sum()/1000000
			cell.value = val
			cell.number_format = '0.00'
			cell.font = Font(name='Arial', size=8)
			cell.border = Border(top=Side(border_style='hair'), 
				bottom=Side(border_style='hair'))
			cell.fill = PatternFill(start_color='EEEEEE', end_color='EEEEEE',
				fill_type='solid')
			self.wb.save(EXCEL_NAME)
			row += 1

	def _copy_file(self):
		src = f'{ROOT_DIR}/{EXCEL_NAME}'
		dist = f'{DOWNLOADS_DIR}/{EXCEL_NAME}'
		copyfile(src, dist)

	def write_data(self):
		with open(EXCEL_NAME, 'a+'):
			self._fill_date()
			self._fill_colnum()
			self._fill_nums()
			self._fill_portfolio_nums()
			self._copy_file()
			self.status_code = 200

	def remove_files(self):
		os.remove(self.RAR_NAME)
		os.remove(self.DBF_NAME)


class Dashboard(tk.Frame):
	"""
	Main gui frame for inputing the month/year and
	outputing the proccessing status.
	"""
	def __init__(self, master=None):
		tk.Frame.__init__(self, master, bg='white')
		self.grid()
		self.default_month_year = self._default_month_year()
		self.base_month = None
		self.base_year = None
		self.status_message = '>Привет!'
		self.message_label = None
		self.month_year_new = None
		self._init_top()
		self._init_bottom()
	
	def _default_month_year(self):
		"""
		Defines the preset of the option box.
		"""
		today = datetime.datetime.today()
		month = today.month - 1
		year = str(today.year-1) if month == 12 else str(today.year)
		month = f'0{month}' if month <= 9 else str(month)
		default_month_year = {'month': month, 'year': year}
		return default_month_year
	
	def _update_message_label(self):
		self.message_label.configure(text=self.status_message)
		self.message_label.update()

	def _press_btn(self):
		file_status = FileStatus(self.month_year_new)
		downloaded_months = file_status.downloaded_months

		remove_garbage_files()

		if (self.base_month, self.base_year) in downloaded_months:
			self.status_message =\
				f'>Данные за {self.base_month}/{self.base_year} уже заполнены'
			self._update_message_label()
			return
		self.month_year_new = (self.base_month, self.base_year)
		
		self.status_message = f'>Загрузка данных...'
		self._update_message_label()

		parser = Parser(base_month=self.base_month, base_year=self.base_year)
		parser.download_rar()
		if parser.status_code != 200:
			self.status_message =\
				f'>Данные за {self.base_month}/{self.base_year} не опубликованы'
			self._update_message_label()
			return
		
		parser.extract_rar()
		self.status_message = f'>Заполнение данных...'
		self._update_message_label()
		writer = Writer(base_month=self.base_month, base_year=self.base_year)
		writer.write_data()
		remove_garbage_files()

		self.status_message = f'>Готово! Файл в папке Загрузки.'
		self._update_message_label()

	def _init_top(self):
		def _update_month(*args):
			self.base_month = month.get()
		def _update_year(*args):
			self.base_year = year.get()

		top = tk.Frame(self, width=280, height=110)
		top.grid(pady=5)
		top.grid_propagate(0)
		title_frame = tk.Frame(top, bg='white')
		title_frame.grid(row=0, sticky='W')
		title = tk.Label(title_frame, text='ЦБ 101 ФОРМА', bg='white',
			fg='black', font=("Helvetica", 16))
		title.grid(row=0, padx=5, pady=5)
		
		choice_frame = tk.Frame(top)
		choice_frame.grid(row=1, padx=35, pady=5)
		l1 = tk.Label(choice_frame, text='данные за:')
		l1.grid(row=0, column=0, sticky='W')

		month = tk.StringVar(choice_frame)
		month.trace('w', _update_month)
		month.set(self.default_month_year['month'])
		month_options = tk.OptionMenu(choice_frame, month, *MONTHS)
		month_options.grid(row=0, column=1)

		year = tk.StringVar(choice_frame)
		year.trace('w', _update_year)
		year.set(self.default_month_year['year'])
		year_options = tk.OptionMenu(choice_frame, year, *YEARS)
		year_options.grid(row=0, column=2)

		btn = tk.Button(choice_frame, text='скачать',bg="grey",fg="black",
			command=self._press_btn)
		btn.grid(row=1, sticky='W')
		
	def _init_bottom(self):
		bottom = tk.Frame(self, bg="black", width=280, height=100)
		bottom.grid()
		bottom.grid_propagate(0)
		message_label = tk.Label(bottom, text=self.status_message, bg='black', fg='white')
		self.message_label = message_label
		message_label.grid(row=0, padx=5, pady=5)


if __name__ == "__main__":
	root = tk.Tk()
	root.title('cbank v1.0')
	dash = Dashboard(root)

	root.mainloop()
